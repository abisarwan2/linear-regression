import tkinter as tk
from tkinter import ttk
from tkinter import filedialog as fd
import os
import reglin


def select_file():
    position_current = os.path.dirname(os.path.abspath(__file__))

    filetypes = (
        ('text files', '*.csv'),
    )

    filename = fd.askopenfilename(
        title='Open a file',
        initialdir=position_current,
        filetypes=filetypes)

    reglin.do_magic(filename)


def create_gui():
    root = tk.Tk()
    root.title("Regression Linear")
    root.resizable(False, False)
    root.geometry('300x150')
    open_button = ttk.Button(
        root,
        text='Open a file',
        command=select_file
    )

    open_button.pack(expand=True)

    root.mainloop()


if __name__ == '__main__':
    create_gui()
    select_file()
