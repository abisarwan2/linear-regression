import csv
import matplotlib.pyplot as plt


def slope_m(x, y, n):
    val_xy, val_x2 = 0, 0
    val_x = sum(x)
    val_y = sum(y)

    for i in range(n):
        val_xy = val_xy + (x[i] * y[i])
        val_x2 = val_x2 + (x[i] ** 2)

    sumval_x2 = val_x ** 2

    numerator = (n * val_xy) - (val_x * val_y)
    denominator = (n * val_x2) - sumval_x2

    return numerator / denominator


def intercept_b(x, y, m, n):
    numerator = sum(y) - (m * sum(x))

    return numerator / n


def estimation(x, n, m, b):
    res = []

    for i in range(n):
        temp = m * x[i] + b
        res.append(temp)

    return res


def show_plot(x, y, sym='-'):
    plt.plot(x, y, sym)


def read_file(file):
    with open('advertising.csv') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        col = next(reader)
        if len(col) != 2:
            raise Exception("It must only have 2 column x and y")
        x, y = [], []
        for row in reader:
            x.append(float(row[0]))
            y.append(float(row[1]))
    return x, y


def do_magic(file_name):
    x, y = read_file(file_name)
    m = slope_m(x, y, len(x))
    b = intercept_b(x, y, m, len(x))
    y_estim = estimation(x, len(x), m, b)
    plt.plot(x, y, '*')
    plt.plot(x, y_estim, '-')
    plt.show()
